import Foundation

struct Show {
    let id: Int
    let name: String
}